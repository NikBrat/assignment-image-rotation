#include  <inttypes.h>
#include <stdio.h>

#include "image_creator.h"

#ifndef IMAGE_TRANSFORMER_BMP_READER_H
#define IMAGE_TRANSFORMER_BMP_READER_H

#define BMP_TYPE 0x4d42
#define DIB_SIZE 40
#define BITS_PER_PIXEL 24
#define PLANE_NUMBER 1
#define COMPRESSION 0

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
} __attribute__((packed));

enum bmp_errors {
    READING_ERROR=1,
    WRONG_TYPE,
    SIZE_ERROR,
    WRONG_BITS_PER_PIXEL,
    WRITING_ERROR,
    PADDING_IS_INCORRECT
};

enum bmp_errors bmp_reading(FILE* input, struct bmp_header* src_header);

enum bmp_errors image_fill( const struct image* img, FILE* source_file );

enum bmp_errors bmp_writing(FILE* output, const struct image* image);

#endif //IMAGE_TRANSFORMER_BMP_READER_H
