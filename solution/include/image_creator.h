#include  <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_CREATOR_H
#define IMAGE_TRANSFORMER_IMAGE_CREATOR_H

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

enum image_creation_errors {
    ALLOCATION_ERROR = 1,
    CREATING_ERROR

};

enum image_creation_errors image_creator( uint32_t width, uint32_t height, struct image* image );

#endif //IMAGE_TRANSFORMER_IMAGE_CREATOR_H
