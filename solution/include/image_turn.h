#include  <inttypes.h>
#include <stdio.h>

#include "image_creator.h"

#ifndef IMAGE_TRANSFORMER_IMAGE_TURN_H
#define IMAGE_TRANSFORMER_IMAGE_TURN_H

enum image_creation_errors image_turn( const struct image* src_image, struct image* turned_image);

#endif //IMAGE_TRANSFORMER_IMAGE_TURN_H
