#include "bmp_reader.h"


static enum bmp_errors bmp_check( const struct bmp_header* header ){

    // Checking file type
    if (header->bfType != BMP_TYPE){
        fprintf( stderr, "File type is not BMP." );
        return WRONG_TYPE;
    }

    // Checking image size
    if (header->biHeight * header->biWidth < 1){
        fprintf( stderr, "Image size is incorrect." );
        return SIZE_ERROR;
    }

    // Checking number of bits per pixel
    if (header->biBitCount != 24){
        fprintf( stderr, "Number of bits per pixel isn't equal to 24." );
        return WRONG_BITS_PER_PIXEL;
    }

    return 0;
}


enum bmp_errors bmp_reading( FILE* input, struct bmp_header* src_header){

    // Reading bmp header
    const size_t f = fread( src_header, sizeof(struct bmp_header), 1, input);

    // Checking if the header was read
    if (f != 1){
        fprintf( stderr, "Error occurred while reading bmp header of source file." );
        return READING_ERROR;
    }

    // Checking the header
    if (bmp_check( src_header )){
        return READING_ERROR;
    }

    return 0;
}

static uint8_t padding_number( const uint32_t width ){

    const uint32_t extra_bytes = (width*sizeof( struct pixel ))%4;
    if (extra_bytes){
        return (4-extra_bytes);
    }
    return 0;

}

static struct bmp_header header_creator(const struct image* image){

    struct bmp_header header={0};
    //Creating header
    header.bfType = BMP_TYPE;
    header.biSizeImage = (sizeof(struct pixel) * image->width + padding_number( image->width )) * image->height;
    header.bfileSize = header.biSizeImage + sizeof( struct bmp_header );
    header.bOffBits = (uint32_t) sizeof( struct bmp_header );
    header.biSize = DIB_SIZE;
    header.biHeight = image->height;
    header.biWidth = image->width;
    header.biPlanes = PLANE_NUMBER;
    header.biBitCount = BITS_PER_PIXEL;
    header.biCompression = COMPRESSION;

    return header;
}

enum bmp_errors image_fill( const struct image* img, FILE* source_file ){

    const uint32_t height = img->height;
    const uint32_t width = img->width;

    // Counting the padding bytes
    const uint32_t padding_bytes = padding_number(img->width);

    // Reading an image
    for (uint32_t row = 0; row < height; row++){
        size_t f = fread (img->data + row*width, sizeof( struct pixel ), width, source_file);
        if (f != width){
            fprintf(stderr, "Something is wrong with reading image row.");
            return READING_ERROR;
        }

        // Reading and checking the padding bites number
        uint32_t rubbish = 0;
        if (fread( &rubbish, 1, padding_bytes, source_file ) != padding_bytes){
            fprintf( stderr, "Something is wrong with the padding.");
            return PADDING_IS_INCORRECT;
        }
    }
    return 0;
}


enum bmp_errors bmp_writing(FILE* output, const struct image* image){
    // Creating header
    struct bmp_header out_header = {0};
    out_header = header_creator(image);

    // Writing header to file
    if (fwrite( &out_header, sizeof( struct bmp_header ), 1, output )!=1){
        fprintf(stderr, "Something is wrong with header writing");
        return WRITING_ERROR;}

    // Writing image to file
    const uint32_t height = image->height;
    const uint32_t width = image->width;

    // Counting the padding bytes
    const uint8_t padding_bytes = padding_number(image->width);
    for (uint32_t row = 0; row < height; row++){

        // Writing pixels to the file
        if (fwrite(image->data + row*width, sizeof( struct pixel ), width, output) != width){
            fprintf(stderr, "Something is wrong with image writing");
            return WRITING_ERROR;
        }

        //  Writing padding bytes to the file
        const uint32_t rubbish = 0;
        if (fwrite( &rubbish, 1, padding_bytes, output ) != padding_bytes){
            fprintf(stderr, "Something is wrong with the padding writing.");
            return WRITING_ERROR;
        }
    }

    return 0;
}
