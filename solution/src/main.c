#include "bmp_reader.h"
#include "image_creator.h"
#include "image_turn.h"

#define READ "rb"
#define WRITE "wb"

enum main_errors {
    SUCCESSFUL_COMPLETE,
    ARGUMENTS_NUMBER,
    OPENING_ERROR,
    READING_BMP_ERROR,
    CREATING_IMAGE_ERROR,
    FILLING_IMAGE_ERROR,
    TURNING_ERROR,
    WRITING_BMP_ERROR

};


int main( int argc, char** argv ) {

    // Checking number of  input arguments
    if (argc != 3){
        fprintf( stderr, "Wrong number of input arguments. Expected 2: <source-image> <transformed-image>" );
        return ARGUMENTS_NUMBER;
    }

    // Opening source file
    FILE* src_file = fopen( argv[1], READ );
    if (src_file == NULL) {
        fprintf( stderr, "Cannot open source file <%s>. Finishing the program", argv[1] );
        return OPENING_ERROR;
    }

    // Opening output file
    FILE* out_file = fopen( argv[2], WRITE );
    if (out_file == NULL) {
        fprintf( stderr, "Cannot open output file <%s>. Finishing the program", argv[2] );
        fclose( src_file );
        return OPENING_ERROR;
    }

    // Reading bmp-header of the source file
    struct bmp_header src_header = {0};
    if (bmp_reading( src_file, &src_header )){
        fclose( src_file );
        fclose( out_file );
        return READING_BMP_ERROR;
    }

    // Getting the src_image
    struct image src_img = {0};

    if (image_creator( src_header.biWidth, src_header.biHeight, &src_img )){
        free( src_img.data );
        fclose( src_file );
        fclose( out_file );
        return CREATING_IMAGE_ERROR;
    }

    else{
        if (image_fill( &src_img, src_file )){
            free( src_img.data );
            fclose( src_file );
            fclose( out_file );
            return FILLING_IMAGE_ERROR;
        }
    }

    // Turning  image
    struct image turned_img = {0};
    if (image_turn( &src_img, &turned_img )){
        free( src_img.data );
        free( turned_img.data );
        fclose( src_file );
        fclose( out_file );
        return TURNING_ERROR;
    }

    // Saving it to the output file
    else{
        if (bmp_writing( out_file, &turned_img )){
            free( src_img.data );
            free( turned_img.data );
            fclose( src_file );
            fclose( out_file );
            return WRITING_BMP_ERROR;
        }
    }

    // Freeing memory and closing files
    free( src_img.data );
    free( turned_img.data );
    fclose( src_file );
    fclose( out_file );


    return SUCCESSFUL_COMPLETE;
}
