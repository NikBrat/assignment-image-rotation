#include "image_turn.h"


enum image_creation_errors image_turn(const struct image* src_image, struct image* turned_image){

    const uint32_t src_width = src_image->width;
    const uint32_t src_height = src_image->height;

    // Creating turned image
    if (image_creator( src_height, src_width, turned_image )){
        fprintf(stderr, "Something went wrong while creating turned image.");
        return CREATING_ERROR;
    }

    // Filling the turned image
    for (uint32_t row = 0; row < src_height; row++){
        for (uint32_t column = 0; column < src_width; column++){
            turned_image->data[src_height*(column+1) - row - 1] = src_image->data[row*src_width + column];

        }
    }
    return 0;
}
