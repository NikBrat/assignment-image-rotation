#include "image_creator.h"


enum image_creation_errors image_creator(const uint32_t width, const uint32_t height, struct image* image ){

    // Inserting data
    image->width = width;
    image->height = height;

    // Creating array for pixels
    image->data = (struct pixel*) malloc(sizeof( struct pixel )*width*height);
    if (!image->data){
        fprintf( stderr, "Memory allocation is failed." );
        return ALLOCATION_ERROR;
    }
    return 0;

}
